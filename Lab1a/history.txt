mkdir tmp
cd tmp
touch tmp.txt
vi tmp.txt
mv tmp.txt name.txt
ls
wc -l name.txt
grep o name.txt
grep mas name.txt
grep o[lm] name.txt
man grep
grep -v o name.txt
cd ..
man du
du tmp
ls -l tmp
ls -lah tmp
clear
rm -r tmp
history
