
# coding: utf-8

# In[ ]:

def sym_gauss_int_sqr(xb,n):
    import numpy as np
    summ = 0
   
    neg_xb = -1 * xb
    L_pointer = neg_xb
    width = (2 * xb) / n
    i = neg_xb
    R_pointer = 0
    while i < xb : 
        R_pointer = L_pointer + width
        R_val = np.exp(-1 *(R_pointer**2))
        L_val = np.exp(-1 *(L_pointer**2))
        avg = (R_val + L_val) / 2
        summ += avg * width
        L_pointer += width
        i += width
    sum_sqr = summ *summ
    return sum_sqr
